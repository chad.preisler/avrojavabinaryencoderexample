# avroJavaBinaryEncoderExample

This project is an example of how to share messages written with the Avro Java class
`org.apache.avro.message.BinaryMessageEncoder` with applications written in Python. 
The Avro Java class writes messages with a header that contains an Avro 64-bit 
Rabin Fingerprint to identify the schema associated with the message. As of 
Avro 1.10.1 there is no support in Python for creating the canonical form of a schema. There
is also no support in Python for generating Rabin 64-bit fingerprints. This code implements
those features in Python so that the messages can be shared.

# Requirements

* Python 3.8.10 or above
* Java 16 or above

The Java portion of the project was written with JDK 16 and Avro 1.10.2. This
code will most likely work with older versions of Java and Avro.

The Python code was written with Python 3.8.10 and was briefly test with 3.9.7.
You will need to install the `requests` and `avro` modules.
Create a shared directory somewhere on your home machine. 
You should copy the `avroTestSchema.avsc` and `avroTestSchema_updated.avsc` located 
in the root directory of this
project into that shared directory. The Java and Python examples read and write to that
directory. The java example should be run first, then run the unit tests for the
Python code.

There is a python script called http_schema_repo_integration_test.py. That code
test to make sure the HttpSchemaStore code works. For that code to run correctly
you need a very simple schemaManager running that can accept HTTP get requests and
return a String representation of a schema.

#Java Project

This project is a maven project and should open up in any Java IDE that supports
maven. However, you can use the following commands to build and run the code.

```
maven clean package
java -jar target/avroJavaBinaryEncoderExample-1.0-SNAPSHOT-jar-with-dependencies.jar
```

# Python code

The Python code is in the `src/main/python` directory. There is a module called
`cpavro` that has the code to encode and decode messages that are compatible
with the Java BinaryMessageDecoder and BinaryMessageEncoder classes. To run the 
unit test use the following command from the `src/main/python` directory.

```
src/main/python
```
