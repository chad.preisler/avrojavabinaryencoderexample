"""
 Copyright (C) 2021 Chad Preisler
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
The goal of this example is to test integrating with an HttpSchemaStore.
This code is aware of one schema, but the binary output written by
java (file java_binary_updated_output.avo) was written with a slightly
different schema. This forces the BinaryMessageDecoder to call the
HttpSchemaStore to retrieve the correct schema for deserialization.

The HttpSchemaStore is an implementation of SchemaStore which mimics
the Java inteface.
"""
import context
from cpavro.io import BinaryMessageDecoder
from cpavro.io import HttpSchemaStore

# Change the directory to match your local machine.
SHARED_DIRECTORY = "/home/chad/app_shared_resources/avroBinaryEncoderTest/"
schema_file = open(SHARED_DIRECTORY + "java_binary_updated_output.avo", "rb")
schema_content = schema_file.read()
schema_store = HttpSchemaStore("http://localhost:8080/schemamanager/schema/") 
to_schema = schema_store.find_by_fingerprint(-2141058657279433257)
schema_decoder = BinaryMessageDecoder(to_schema, schema_store)
order = schema_decoder.decode(schema_content)
print(order)