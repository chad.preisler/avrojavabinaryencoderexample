"""
 Copyright (C) 2021 Chad Preisler
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from cpavro.schema_normalization import SchemaNormalization
from cpavro.io import BinaryMessageEncoder
from cpavro.io import BinaryMessageDecoder
import unittest
import cpavro.io
import avro.schema
import csv

class TestBinaryMessageDecoder(unittest.TestCase):

    def setUp(self):
        self.expected_fingerprint = -2141058657279433257
        SHARED_DIRECTORY = "/home/chad/app_shared_resources/avroBinaryEncoderTest/"
        self.SHARED_DIRECTORY = SHARED_DIRECTORY
        java_file = open(SHARED_DIRECTORY + "java_binary_output.avo", "rb")
        self.java_binary_data = java_file.read()
        parsing_form_file = open(SHARED_DIRECTORY + "java_parsing_form.avsc", "r")
        java_file.close()        
        self.parsing_form_schema = parsing_form_file.read()
        parsing_form_file.close()
        schema_file = open(SHARED_DIRECTORY + "avroTestSchema.avsc", "rb")
        self.schemaBytes = schema_file.read()
        schema_file.close()

    def testDecodeHeader(self):
        schema =  avro.schema.parse(self.schemaBytes)
        decoder = cpavro.io.BinaryMessageDecoder(schema)
        fingerprint, message = decoder.decodeHeader(self.java_binary_data)
        self.assertEqual(self.expected_fingerprint, fingerprint)

    def testDecodeJavaMessage(self):
        schema =  avro.schema.parse(self.schemaBytes)
        orderSchema = self.find_order_schema(schema)
        decoder = cpavro.io.BinaryMessageDecoder(orderSchema)
        decoded = decoder.decode(self.java_binary_data)
        print("Decoded message: ")
        print(decoded)
        self.assertEqual("12345", decoded["recipient"]["customerId"])
        self.assertEqual(123.5, decoded["items"][0]["price"])

    def testEncodeBinaryMessage(self):
        schema =  avro.schema.parse(self.schemaBytes)
        orderSchema = self.find_order_schema(schema)
        order = {
            "recipient": {
                "customerId": "12345",
                "email": "tester@test.com",
                "firstName": "Chad",
                "lastName": "P",
                "addressList": [
                    {
                        "street1": "123 Test Street",
                        'street2': None,
                        "city": "Testville",
                        "state": "WI",
                        "zip": "12345",
                        "addressType": "MAILING"
                    }
                  ],
                  'alerts': {}, 'aFixedField': None
            },
            "items": [
                {
                    "sku": "123-567",
                    "quantity": 2,
                    "price": 123.50
                }
            ]
        }
        encodedMessage = BinaryMessageEncoder.encode(orderSchema, order)
        decoder = BinaryMessageDecoder(orderSchema)
        fingerprint, message = decoder.decodeHeader(encodedMessage)
        self.assertEqual(self.expected_fingerprint, fingerprint)
        self.assertEqual(b'\xC3\x01', encodedMessage[0:2])
        self.assertEqual(self.java_binary_data[0:10], encodedMessage[0:10])
        py_file = open(self.SHARED_DIRECTORY + "python_binary_output.avo", "wb")
        py_file.write(encodedMessage)
        py_file.close()

    def testSchemaNormalization(self):
        schema =  avro.schema.parse(self.schemaBytes)       
        self.assertEqual(self.parsing_form_schema, SchemaNormalization.to_parsing_form(schema))

    def testCalculateFingerprint(self):
        schema =  avro.schema.parse(self.schemaBytes)
        order_schema = self.find_order_schema(schema)
        canonical = bytes(SchemaNormalization.to_parsing_form(order_schema), 'utf-8')
        print(canonical)
        fingerprint = SchemaNormalization.fingerprint_64(canonical)        
        self.assertEqual(self.expected_fingerprint, fingerprint)

    def testCalculateAllFingerprints(self):
        schema =  avro.schema.parse(self.schemaBytes)
        schema_list_file = self.SHARED_DIRECTORY + "fingerPrints.csv"
        with open(schema_list_file, newline='') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                fullname = row[0]
                expected_fingerprint = int(row[1])
                for aSchema in schema.schemas:
                   if (fullname == aSchema.fullname):
                       canonical = bytes(SchemaNormalization.to_parsing_form(aSchema), 'utf-8')
                       fingerprint = SchemaNormalization.fingerprint_64(canonical)        
                       self.assertEqual(expected_fingerprint, fingerprint)
                       break

    def testParsingFingerprints(self):
            schema =  avro.schema.parse(self.schemaBytes)
            schema_list_file = self.SHARED_DIRECTORY + "fingerPrints.csv"
            with open(schema_list_file, newline='') as csvfile:
                reader = csv.reader(csvfile)
                for row in reader:
                    fullname = row[0]
                    expected_fingerprint = int(row[1])
                    for aSchema in schema.schemas:
                        if (fullname == aSchema.fullname):
                            fingerprint = SchemaNormalization.parsing_fingerprint_64(aSchema)        
                            self.assertEqual(expected_fingerprint, fingerprint)
                            break

    def testBadHeader(self):
        decoder = cpavro.io.BinaryMessageDecoder(None)
        self.assertRaises(cpavro.io.BadHeaderError, decoder.decodeHeader, b'1')

    def testFindByFingerprint(self):
        decoder = cpavro.io.HttpSchemaStore("http://test/schema")
        decoder.make_http_request = lambda x: self.schemaBytes
        schema = decoder.find_by_fingerprint(self.expected_fingerprint)
        self.assertIsInstance(schema, avro.schema.Schema, "Should be an instance of avro Schema")

    def testBadSchemaFindByFingerprint(self):
        decoder = cpavro.io.HttpSchemaStore("http://test/schema")
        decoder.make_http_request = lambda x: b'bad scheama'
        schema = decoder.find_by_fingerprint(self.expected_fingerprint)
        self.assertIsNone(schema)


    def find_order_schema(self, schema):
        order_schema = None
        for aSchema in schema.schemas:
            if hasattr(aSchema, "name") and aSchema.name == "Order":
               order_schema = aSchema 
        return order_schema