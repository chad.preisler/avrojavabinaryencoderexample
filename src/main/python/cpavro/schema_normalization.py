"""
 Copyright (C) 2021 Chad Preisler
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import struct

class SchemaNormalization:
    """ Loosly based on the Java class of the same name. Includes methods
    to get fingerprint and generate a fingerprint from a avro.Schema.
    """

    @staticmethod
    def parsing_fingerprint_64(schema):
        """ Pass in a avro.Schema and get back the fingerprint as a long.
        """
        canonical = bytes(SchemaNormalization.to_parsing_form(schema), 'utf-8')
        return SchemaNormalization.fingerprint_64(canonical)

    @staticmethod
    def fingerprint_64(data):
        """ Pass in a byte array that represents the schema.
        Return fingerprint as long
        """
        empty_64 = struct.unpack('>q', bytearray.fromhex('C15D213AA4D7A795'))[0]
        fp_table = []
        for i in range(256):
            fp = i
            for j in range(8):
                mask = -(fp & 1)
                unsigned_shift = (fp & 0xFFFFFFFFFFFFFFFF) >> 1 
                fp = (unsigned_shift) ^ (empty_64 & mask)
            fp_table.append(fp)

        result = empty_64
        for byte in data:
            unsigned_shift = (result & 0xFFFFFFFFFFFFFFFF) >> 8
            result = (unsigned_shift) ^ fp_table[(result ^ byte) & 0xFF]
        
        return result

    @staticmethod
    def to_parsing_form(schema):
        env = {}
        #schema_wrapper = {"type": schema.type, "schema": schema}
        parsedSchema = SchemaNormalization.build(env, schema)
        return json.dumps(parsedSchema, separators=(',', ':'))

    @staticmethod
    def build(env : dict, schema):    
        schema_type = schema.type
        if hasattr(schema, "fullname"):
            name = schema.fullname
            if name in env:
                return name
            env[name] = name
        else:
            name = schema.type

        if "union" == schema_type:
            return SchemaNormalization.union(env, schema)
        elif "array" == schema_type:
            items = schema.get_prop("items")
            array_obj = {"type": "array", "items": SchemaNormalization.build(env, items)}
            return array_obj
        elif "map" == schema_type:
            values = schema.get_prop("values")
            map_obj = {"type": "map", "values": SchemaNormalization.build(env, values)}          
            return map_obj
        elif "enum" == schema_type:
            enum_obj = SchemaNormalization.open_complex_type(env, name, schema.type)
            enum_obj["symbols"] = schema.get_prop("symbols")  
            return enum_obj          
        elif "fixed" == schema_type:
            fixed_obj = SchemaNormalization.open_complex_type(env, name, schema.type)
            fixed_obj["size"] = schema.get_prop("size")
            return fixed_obj
        elif "record" == schema_type:
            record_obj = SchemaNormalization.open_complex_type(env, name, schema.type)
            schema_fields = schema.get_prop("fields")
            fields = SchemaNormalization.open_complex_type(env, name, schema.type)
            fields_list = []
            for field in schema_fields:
                fields_list.append({"name": field.name, "type": SchemaNormalization.build(env, field.type)})
            record_obj["fields"] = fields_list
            return record_obj
        else:
            return schema_type

    @staticmethod
    def open_complex_type(env, name, type):
        json_obj = {
            "name": name,
            "type": type
        }
        env[name]=name
        return json_obj
       

    @staticmethod
    def union(env: dict, schema):
        union_types = []        
        for aSchema in schema.schemas:    
            union_types.append(SchemaNormalization.build(env, aSchema))
        return union_types
        