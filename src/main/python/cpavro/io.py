"""
 Copyright (C) 2021 Chad Preisler
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import avro.io
import abc
import avro.schema
import io
import requests
import logging
from cpavro.schema_normalization import SchemaNormalization

class BinaryMessageEncoder:
    """
    This is a port of the Java class with the same name. It encodes a message
    with a header that is compatibile with the Java implementation.

    A MessageEncoder that adds a header and 8-byte schema fingerprint to each datum encoded as binary.
    """
    @staticmethod
    def encode (schema, message):
        """
            Given a schema encode the message passed in writing the Java compatible
            binary header.
        """
        fingerprint = SchemaNormalization.parsing_fingerprint_64(schema)
        writer = avro.io.DatumWriter(schema)
        bytes_writer = io.BytesIO()
        encoder = avro.io.BinaryEncoder(bytes_writer)
        writer.write(message, encoder)
        encodedMessage = b'\xC3\x01' + fingerprint.to_bytes(8, 'little', signed=True) + bytes_writer.getvalue()
        return encodedMessage    

class BinaryMessageDecoder:
    """ A port of the Java class by the same name. This class will decode messages written
        with the Java BinaryMessageDcoder or by the BinaryMessageEncoder found in this
        module.
        """

    def  __init__ (self, schema, schema_store=None):
        """ Uses the schema passed in as the writers schema.
        Typically this is the schema your code is working with. The
        schema_store argument will be used to find schemas that
        have a matching fingerprint for the readers schema.

        :parameter schema: Expecting an instance of avro.schema
        :parameter schema_store: Expecting an instance of cpavro.io.SchemaStore
        """
        self.schema = schema
        self.schema_store = schema_store
        
    def decode (self, message_content):
        """Will try to decode the avro message using the schema_store
        if available.

        :parameter message_content: The avro encoded message.
        """
        fingerprnt, message_body = self.decodeHeader(message_content)
        reader = io.BytesIO(message_body)
        decoder = avro.io.BinaryDecoder(reader)
        writers_schema = None
        if (self.schema_store is not None):
            writers_schema = self.schema_store.find_by_fingerprint(fingerprnt)
        if (writers_schema is None):
            writers_schema = self.schema
        if (writers_schema is None):
            logging.warning("Could not pull a schema from SchemaStore, and no schema set in BinaryMessageDecoder.")
            retValue = None
        else:
            datum_reader = avro.io.DatumReader(writers_schema, self.schema)
            retValue = datum_reader.read(decoder)
        return retValue

    def decodeHeader(self, message):
        if (message[0:2] == b'\xC3\x01'):                       
            fingerprint = int.from_bytes(message[2:10], "little", signed=True)
            message_body = message[10:]
        else:
            raise BadHeaderError("First two bytes of header were bad.")
        return (fingerprint, message_body)

class SchemaStore(metaclass=abc.ABCMeta):
    """ Retrieves a fingerprint by its AVRO-CRC-64 fingerprint.
    """
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'find_by_fingerprint') and 
                callable(subclass.find_by_fingerprint))


class HttpSchemaStore:
    """ Uses a supplied URL to find a schema by fingerprint.
    The fingerprint is appended to the end of the URL as part of the path.
    """

    def __init__ (self, url, request_headers=None):
        """ Pass in the base URL to call wabchen a fingerprint is not found locally.

        :parameter url: the base URL for finding a schema.
        :parameter request_headers: dict with any special headers to add to the request
        """
        self.url = url
        self.request_headers = request_headers

    def find_by_fingerprint(self, fingerprint: int):
        """ Pass in the AVRO-CRC-64 fingerprint. This returns a tuple
        that has a boolean for found and the avro schema object (if it was found).
        """
        schema = None
        binary_schema = self.make_http_request(fingerprint)
        if (binary_schema is not None):
            try:
                schema = avro.schema.parse(binary_schema)
            except avro.schema.SchemaParseException as error:
                logging.error("Ivalid schema returned from request.", error)
        return schema 

    def make_http_request(self, fingerprint):
        request_url = self.url + str(fingerprint)
        try:
            if (self.request_headers is not None):
                response = requests.get(request_url, headers=self.request_headers)
            else:
                response = requests.get(request_url)
            if response.status_code == requests.codes.ok:
                return response.content                
        except requests.exceptions.ConnectionError as error:
            logging.error("Unable to connect to schema manager.", error)
        return None


class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class BadHeaderError(Error):
    """Exception raised for errors parsing the header.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message