/*
 * Copyright (C) 2021 Chad Preisler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package chad.preisler.avro.examples;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.avro.Schema;
import org.apache.avro.SchemaNormalization;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.BinaryMessageEncoder;

/**
 *
 * @author Chad Preisler
 */
public class AvroWriteReadBinary {

    public static final String SHARED_DIRECTORY = "/home/chad/app_shared_resources/avroBinaryEncoderTest/";
    public static final String AVRO_SHEMA_PATH = SHARED_DIRECTORY + "avroTestSchema.avsc";
    public static final String AVRO_UPDATED_SHEMA_PATH = SHARED_DIRECTORY + "avroTestSchema_updated.avsc";


    public static void main(String[] args) {
        try {
            var schemaContent = Files.readAllBytes(Paths.get(AVRO_SHEMA_PATH));
            Schema schema = new Schema.Parser().parse(new String(schemaContent));
            var orderSchema = findSchema(schema.getTypes(), "py.java.test.Order");
            printAllFingerprints(schema.getTypes());
            Files.write(Paths.get(SHARED_DIRECTORY + "java_parsing_form.avsc"), SchemaNormalization.toParsingForm(schema).getBytes());
            System.out.println(SchemaNormalization.toParsingForm(orderSchema));
            System.out.println("fingerprint64 " + SchemaNormalization.fingerprint64(orderSchema.toString().getBytes()));
            System.out.println("parsingFingerprint64 " + SchemaNormalization.parsingFingerprint64(orderSchema));

            System.out.println("Loaded Schema: " + schema + " with fingerprint " + SchemaNormalization.parsingFingerprint64(schema));

            System.out.println("Writing the file out ...");
            
            GenericRecord message = buildOrder(schema);
            var binaryEncoder = new BinaryMessageEncoder<GenericRecord>(new GenericData(), orderSchema);
            var encoded = binaryEncoder.encode(message).array();
            String outputFilePath = SHARED_DIRECTORY + "java_binary_output.avo";
            Files.write(Paths.get(outputFilePath), encoded);
            System.out.println("Wrote file to " + outputFilePath);

            System.out.println("Reading file back in...");
            byte[] encodedRead = Files.readAllBytes(Paths.get(outputFilePath));
            var decoder = new BinaryMessageDecoder<GenericData.Record>(new GenericData(), orderSchema);
            GenericData.Record decoded = decoder.decode(encodedRead);
            System.out.println("Read record in: " + decoded);
            if (Files.exists(Paths.get(SHARED_DIRECTORY + "python_binary_output.avo"))) {
                byte[] encodedFileFromPython = Files.readAllBytes(Paths.get(SHARED_DIRECTORY + "python_binary_output.avo"));
                var decodedFromPyton = decoder.decode(encodedFileFromPython);
                System.out.println("Written by Python: " + decodedFromPyton);
            }
            // write a record using the an updated schema that contains an additional optional field.
            var updatedSchemaContent = Files.readAllBytes(Paths.get(AVRO_UPDATED_SHEMA_PATH));            
            Schema updatedSchema = new Schema.Parser().parse(new String(updatedSchemaContent));
            var updatedOrderSchema = findSchema(schema.getTypes(), "py.java.test.Order");
            var updatedOrderMessage = buildOrder(updatedSchema);
            updatedOrderMessage.put("orderId", "56789");
            outputFilePath = SHARED_DIRECTORY + "java_binary_updated_output.avo";
            binaryEncoder = new BinaryMessageEncoder<GenericRecord>(new GenericData(), updatedOrderSchema);
            encoded = binaryEncoder.encode(updatedOrderMessage).array();
            Files.write(Paths.get(outputFilePath), encoded);
            System.out.println("Wrote file to " + outputFilePath);
        } catch (IOException ex) {
            Logger.getLogger(AvroWriteReadBinary.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static Schema findSchema(List<Schema> schemas, String name) {
        return schemas.stream().filter(schema -> name.equals(schema.getFullName())).findFirst().get();
    }
    
    static void printAllFingerprints(List<Schema> schemas) {
        var file = new File(SHARED_DIRECTORY + "fingerPrints.csv");
        try (var writer = new PrintWriter(file)) {
            schemas.stream().forEach(schema -> {
                writer.println(String.format("%s,%s", schema.getFullName(),SchemaNormalization.parsingFingerprint64(schema)));
            });
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AvroWriteReadBinary.class.getName()).log(Level.SEVERE, "Could not write list of fingerprints", ex);
        }
    }

    static GenericRecord buildUser(Schema fullSchema) {
        var addressSchema = findSchema(fullSchema.getTypes(), "py.java.test.Address");
        var address = new GenericData.Record(addressSchema);
        address.put("street1", "123 Test Street");
        address.put("city", "Testville");
        address.put("state", "WI");
        address.put("zip", "12345");
        var addressTypeSchema = findSchema(fullSchema.getTypes(), "py.java.test.AddressType");
        var addressType = new GenericData.EnumSymbol(addressTypeSchema, "MAILING");
        address.put("addressType", addressType);

        var userSchema = findSchema(fullSchema.getTypes(), "py.java.test.User");
        var user = new GenericData.Record(userSchema);
        user.put("customerId", "12345");
        user.put("email", "tester@test.com");
        user.put("firstName", "Chad");
        user.put("lastName", "P");
        List<GenericRecord> addressList = new ArrayList();
        addressList.add(address);
        user.put("addressList", addressList);
        user.put("alerts", new TreeMap<String, String>());
        return user;
    }

    static GenericRecord buildItem(Schema fullSchema) {
        var itemSchema = findSchema(fullSchema.getTypes(), "py.java.test.Item");
        GenericRecord item = new GenericData.Record(itemSchema);
        item.put("sku", "123-567");
        item.put("quantity", 2);
        item.put("price", 123.50);
        return item;
    }

    static GenericRecord buildOrder(Schema fullSchema) {
        var orderSchema = findSchema(fullSchema.getTypes(), "py.java.test.Order");
        GenericRecord order = new GenericData.Record(orderSchema);
        var user = buildUser(fullSchema);
        var item = buildItem(fullSchema);
        List<GenericRecord> items = new ArrayList();
        items.add(item);
        order.put("recipient", user);
        order.put("items", items);
        return order;
    }
}
